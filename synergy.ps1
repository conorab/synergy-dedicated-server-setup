# Note the command immediately below this comment has been modified a re-used in a million other places due to it's usefulness in testing scripts in the Powershell ISE since the ISE doesn't delete variables after executing the script.
# This code was not copied from any other scripts I have written, but has been copied to countless other scripts I have written over the years.

"serviceAccount","servicePassword","steamUsername","steamPassword","serviceAccountHome","serviceAccountHome","steamcmdLocation","srcdsPath","srcdsArguments","servicePassword_secureString","serviceCredentials","serviceName","srcdsMap","srcdsHostname","srcdsRConPassword","srcdsServerPassword","action","trigger","principal","commandToRun","srcdsRegion" | ForEach {
    if (Get-Variable -Name $_ -ErrorAction Ignore) {
        Remove-Variable -Force -Name $_
    } 
}

$serviceName="Synergy Server"
$serviceAccount='steam'
$servicePassword=""
$steamUsername=""
$steamPassword=""
$serviceAccountHome='C:\Users\steam'
$steamcmdLocation=$serviceAccountHome + '\steamcmd\steamcmd.exe'
$srcdsPath=$serviceAccountHome + '\steamcmd\steamapps\common\Synergy\srcds.exe'
$srcdsMap="d1_trainstation_01"
$srcdsHostname=$serviceName
$srcdsRegion="1"
$srcdsRConPassword=""
$srcdsServerPassword=""
$srcdsArguments='-console -game synergy +map "' + $srcdsMap + '" -nocrashdialog -nohltv +hostname "' + $srcdsHostname + '" +hostport 27015 +rcon_password "' + $srcdsRConPassword + '" +sv_password "' + $srcdsServerPassword + '" +sv_allowupload 1 +sv_allowdownload 1 +sv_region "' + $srcdsRegion + '" +log 1 +sv_logbans 1 +sv_rcon_minfailures 2 +sv_rcon_maxfailures 3 +sv_rcon_banpenalty 0 +sv_rcon_minfailuretime 30 +sv_pure 0 +mp_friendlyfire 0 +sv_cheats 1 +sv_lan 0'

$servicePassword_secureString=ConvertTo-SecureString "$servicePassword" -AsPlaintext -Force
$serviceCredentials=New-Object -typename System.Management.Automation.PSCredential -ArgumentList $serviceAccount, $servicePassword_secureString

New-LocalUser -AccountNeverExpires -Name $serviceAccount -Password $servicePassword_secureString -PasswordNeverExpires
Start-Process powershell.exe -Credential $serviceCredentials -NoNewWindow -Wait -ArgumentList '-Command ls'
# Adapted from: http://www.sysadmins.eu/2014/12/set-up-auto-logon-in-windows-using.html
New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon' -Name AutoAdminLogon -Value 1 -ErrorAction SilentlyContinue
New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon' -Name DefaultUserName -Value $serviceAccount -ErrorAction SilentlyContinue
New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon' -Name DefaultPassword -Value $servicePassword -ErrorAction SilentlyContinue

$action =@()
$action += New-ScheduledTaskAction -Execute $srcdsPath -Argument $srcdsArguments
$trigger =  New-ScheduledTaskTrigger -AtLogOn -User $serviceAccount
$principal=New-ScheduledTaskPrincipal -LogonType Interactive -UserId steam 
Register-ScheduledTask -Action $action -Trigger $trigger -Principal $principal -TaskName $serviceName


Write-Host "Waiting for the service account home directory to exist..."
while (-Not (Test-Path $serviceAccountHome)) {

	sleep 1
}
cd $serviceAccountHome

Start-Process powershell.exe -Credential $serviceCredentials -NoNewWindow -Wait -ArgumentList '-Command "wget -OutFile steamcmd.zip media.steampowered.com/installer/steamcmd.zip"'

Start-Process powershell.exe -Credential $serviceCredentials -NoNewWindow -Wait -ArgumentList '-Command "Expand-Archive steamcmd.zip"'

$commandToRun='/C ' + $steamcmdLocation + ' +login ' + "$steamUsername" + ' ' + "$steamPassword" + ' +app_update 17520 validate +app_update 220 validate +app_update 380 validate +app_update 420 validate +app_update 340 validate +quit'
Start-Process cmd.exe -Credential $serviceCredentials -NoNewWindow -Wait -ArgumentList $commandToRun

New-NetFirewallRule -DisplayName $serviceName -Name $serviceName -Profile Any -Direction Inbound -Action Allow -Program $srcdsPath -Enabled True
